# -*- coding: utf-8 -*-
import scrapy

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector

import sys, getopt, re, hashlib, os
from urlparse import urlparse

def get_domain(url) :
    domain = urlparse(url).netloc
    return re.sub('^www\.', '', domain);


class RecurseSpider(CrawlSpider):
    name = "recurse"
    
    # load args
    def __init__ (self, *args, **kwargs):
        super(RecurseSpider, self).__init__(*args, **kwargs) 
        self.start_urls = [kwargs.get('start_url')]
        self.allowed_domains = []
        for url in self.start_urls :
            self.allowed_domains.append(get_domain(url))
    
    # set up rules
    rules = (Rule (SgmlLinkExtractor(allow=("", ),restrict_xpaths=('//a',))
    , callback="parse_items", follow= True),
    )

    # parse items
    def parse_items(self, response):
        #
        print response.url
        
        # get file path to cache into
        path = get_domain(response.url) + '-' + hashlib.md5(response.url).hexdigest() + '.html';
        
        # write file
        f = open('./data/' + path, 'w+')
        f.write(response.body)
        f.close()

        return()
