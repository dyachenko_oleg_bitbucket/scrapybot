<?php

/**
 * @return string
 */
function cache_dir() {
  return __DIR__ . '/data';
}

/**
 * @param $dir
 *
 * @return array
 */
function list_dir($dir) {
  $list = scandir($dir);

  foreach ($list as $i => $item) {
    if (preg_match('@^\.+$@', $item)) {
      unset($list[$i]);
    }
    else {
      $list[$i] = $dir . '/' . $item;
    }
  }

  sort($list);

  return $list;
}

/**
 * @param string $text
 */
function timestamp($text = '') {
  static $time;

  if (is_null($time)) {
    $time = microtime(TRUE);
    return;
  }

  $time2 = microtime(TRUE);

  echo round($time2 - $time, 4), "   ", $text, "<br>";

  $time = $time2;
}

/**
 * @param $url
 *
 * @return mixed|string
 */
function get_domain($url) {
  $domain = parse_url($url, PHP_URL_HOST);
  if (0 === strpos($domain, 'www.')) {
    $domain = substr($domain, 4);
  }
  return $domain;
}

/**
 * @return string
 */
function get_scrapy() {
  static $path;

  if (is_null($path)) {
    $path = shell_exec("which scrapy");
  }

  return $path;
}

/**
 * @param      $url
 * @param bool $loaded
 *
 * @return string
 */
function load_url($url, &$loaded = FALSE) {
  $path = cache_dir() . '/' . get_domain($url) . '-' . md5($url) . '.html';
  @mkdir(cache_dir(), 0777, TRUE);

  //
  if (!file_exists($path)) {
    echo $command = sprintf('%s fetch --nolog "%s"', get_scrapy(), str_replace('"', '\"', $url));
    $data = shell_exec($command);
    var_dump($data);
    if ($data) {
      file_put_contents($path, $data);
      $loaded = TRUE;
    }

    else return FALSE;
  }

  return $path;
}